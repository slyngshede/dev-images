FROM {{ "buster" | image_tag }} AS buster

ENV PHP_VERSION=7.2

# Note that WMF APT key is pre-installed.
# See: https://wikitech.wikimedia.org/wiki/APT_repository#Security

RUN echo "deb https://apt.wikimedia.org/wikimedia buster-wikimedia component/php72" \
   > /etc/apt/sources.list.d/wikimedia-php72.list

# Install PHP dependencies:

# Not installing php-tidy here - see https://phabricator.wikimedia.org/T191771
{% set mediawiki_deps|replace('\n', ' ') -%}
php-apcu
php-bcmath
php-cli
php-curl
php7.2-dba
php-gd
php-gmp
php-intl
php-ldap
php-mbstring
php-mysql
php-pgsql
php-sqlite3
php-xml
php-zip
php-redis
php-dev
php-ldap
php-mysql
php-pgsql
build-essential
{%- endset -%}

# Install XDebug but disable it by default due to its performance impact:
RUN {{ mediawiki_deps | apt_install }}

RUN sh /docker/build-xdebug.sh
