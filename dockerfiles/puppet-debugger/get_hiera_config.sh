#!/bin/bash
# Helper script to fetch and update the hiera file
if [ -z "${1}" ]
then
  printf "usage %s path/to/puppet/repo\n" "${0}"
  exit 1
fi
if [ ! -d "${1}" ]
then
  printf "%s: needs to be a valid directory" "${1}"
  exit 1
fi
hiera_file="${1}/modules/puppetmaster/files/hiera/production.yaml"
if [ ! -f "${hiera_file}" ]
then
  printf "unable to find hiera file in expected location: %s\n" "${hiera_file}"
  exit 1
fi
script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
sed 's|/etc/puppet|/etc/puppetlabs|g' "${hiera_file}" > "${script_dir}/hiera.yaml"
